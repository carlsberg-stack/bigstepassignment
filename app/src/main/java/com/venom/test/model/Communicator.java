package com.venom.test.model;

import android.os.Bundle;

import com.android.volley.toolbox.ImageLoader;

import java.util.List;

public interface Communicator {
    List<Media> getMusic();

    List<Media> getVideo();

    List<Media> getHistory();

    void insertMedia(Media media);


    ImageLoader getImageLoader();

    void fragToast(String msg);

    void fragStartActivity(Class<? extends BaseActivity> aClass, Bundle bundle);
}