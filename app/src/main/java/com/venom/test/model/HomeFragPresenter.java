package com.venom.test.model;

import android.content.Context;

import com.venom.test.R;
import com.venom.test.util.ApiClientHandler;
import com.venom.test.util.Constants;
import com.venom.test.util.MyVolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeFragPresenter extends MVPImpl<AppLogic.HomeFragView> implements AppLogic.HomeFragPresenter, ApiClientHandler {
    List<Media> mediaList;
    DatabaseHelper db;

    @Override
    public void getMedia(Context context) {
        if (view == null)
            return;
        view.showProgress(R.string.loading);
        MyVolleyRequest.getInstance(context).get(Constants.URL_MUSIC, this, Constants.RESULT_TYPE_MUSIC);
        MyVolleyRequest.getInstance(context).get(Constants.URL_MUSICVIDEO, this, Constants.RESULT_TYPE_MUSICVIDEO);
    }

    @Override
    public void insertMedia(Media media, Context context) {
        if (db == null)
            db = new DatabaseHelper(context);
        db.insertMedia(media);
    }


    @Override
    public List<Media> getHistory(Context ctx) {
        if (db == null)
            db = new DatabaseHelper(ctx);

        return db.getAllMedia();
    }

    @Override
    public void onSuccess(JSONObject result, int resultType) {
        if (view == null)
            return;
        view.dismiss();
        view.responseMedia(parseResult(result), resultType);
    }

    private List<Media> parseResult(JSONObject result) {
        mediaList = new ArrayList<>();
        try {
            JSONArray jsonArray = result.getJSONArray("results");
            JSONObject response;
            for (int i = 0; i < jsonArray.length(); i++) {
                response = jsonArray.getJSONObject(i);
                mediaList.add(new Media(
                        response.optLong(Constants.trackId),
                        response.optString(Constants.kind),
                        response.optString(Constants.artistName),
                        response.optString(Constants.trackName),
                        response.optString(Constants.collectionName, "Michael Jackson's Vision"),
                        response.optString(Constants.previewUrl),
                        response.optString(Constants.artworkUrl60),
                        response.optDouble(Constants.trackPrice),
                        response.optString(Constants.releaseDate),
                        response.optBoolean(Constants.isStreamable)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mediaList;

    }

    @Override
    public void onFaliure(String error) {
        if (view == null)
            return;
        view.dismiss();
        view.showFailureError(R.string.ptl);
    }
}
