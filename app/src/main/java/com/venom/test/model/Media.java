package com.venom.test.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Media implements Parcelable {

    private long trackId;
    private String kind;
    private String artistName;
    private String trackName;
    private String collectionName;
    private String previewUrl;
    private String artworkUrl60;
    private double trackPrice;
    private String releaseDate;
    private boolean isStreamable;

    public Media(long trackId, String kind, String artistName, String trackName, String collectionName, String previewUrl, String artworkUrl60, double trackPrice, String releaseDate, boolean isStreamable) {
        this.trackId = trackId;
        this.kind = kind;
        this.artistName = artistName;
        this.trackName = trackName;
        this.collectionName = collectionName;
        this.previewUrl = previewUrl;
        this.artworkUrl60 = artworkUrl60;
        this.trackPrice = trackPrice;
        this.releaseDate = releaseDate;
        this.isStreamable = isStreamable;
    }

    protected Media(Parcel in) {
        trackId = in.readLong();
        artistName = in.readString();
        kind = in.readString();
        trackName = in.readString();
        collectionName = in.readString();
        previewUrl = in.readString();
        artworkUrl60 = in.readString();
        trackPrice = in.readDouble();
        releaseDate = in.readString();
        isStreamable = in.readByte() != 0;
    }


    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public String getArtistName() {
        return artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getArtworkUrl60() {
        return artworkUrl60;
    }

    public double getTrackPrice() {
        return trackPrice;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public boolean isStreamable() {
        return isStreamable;
    }

    public long getTrackId() {
        return trackId;
    }

    public String getKind() {
        return kind;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(trackId);
        parcel.writeString(kind);
        parcel.writeString(artistName);
        parcel.writeString(trackName);
        parcel.writeString(collectionName);
        parcel.writeString(previewUrl);
        parcel.writeString(artworkUrl60);
        parcel.writeDouble(trackPrice);
        parcel.writeString(releaseDate);
        parcel.writeByte((byte) (isStreamable ? 1 : 0));
    }
}
