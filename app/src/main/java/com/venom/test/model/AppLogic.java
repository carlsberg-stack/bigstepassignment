package com.venom.test.model;

import android.content.Context;

import java.util.List;

public class AppLogic {

    interface Base extends MVP.BaseView {
        void showFailureError(int message);

        void showProgress(int message);

        void dismiss();
    }

    /********* Logic for home************/
    public interface HomeFragView extends Base {
        void responseMedia(List<Media> media, int mediaType);
    }

    public interface HomeFragPresenter extends MVP.BasePresenter<HomeFragView> {
        void getMedia(Context context);

        void insertMedia(Media media, Context context);

        List<Media> getHistory(Context ctx);
    }
}
