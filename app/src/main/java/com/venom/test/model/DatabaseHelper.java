package com.venom.test.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.venom.test.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "media_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(Constants.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void insertMedia(Media media) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Constants.trackId, media.getTrackId());
        values.put(Constants.kind, media.getKind());
        values.put(Constants.artistName, media.getArtistName());
        values.put(Constants.trackName, media.getTrackName());
        values.put(Constants.collectionName, media.getCollectionName());
        values.put(Constants.previewUrl, media.getPreviewUrl());
        values.put(Constants.artworkUrl60, media.getArtworkUrl60());
        values.put(Constants.trackPrice, media.getTrackPrice());
        values.put(Constants.releaseDate, media.getReleaseDate());
        values.put(Constants.isStreamable, media.isStreamable());

        // insert row
        db.insert(Constants.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
    }


    public List<Media> getAllMedia() {
        List<Media> mediaList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Constants.TABLE_NAME + " ORDER BY " +
                Constants.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                mediaList.add(new Media(
                        cursor.getLong(cursor.getColumnIndex(Constants.trackId)),
                        cursor.getString(cursor.getColumnIndex(Constants.kind)),
                        cursor.getString(cursor.getColumnIndex(Constants.artistName)),
                        cursor.getString(cursor.getColumnIndex(Constants.trackName)),
                        cursor.getString(cursor.getColumnIndex(Constants.collectionName)),
                        cursor.getString(cursor.getColumnIndex(Constants.previewUrl)),
                        cursor.getString(cursor.getColumnIndex(Constants.artworkUrl60)),
                        cursor.getDouble(cursor.getColumnIndex(Constants.trackPrice)),
                        cursor.getString(cursor.getColumnIndex(Constants.releaseDate)),
                        cursor.getInt(cursor.getColumnIndex(Constants.isStreamable)) == 0 ? false : true
                ));
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return mediaList;
    }
}