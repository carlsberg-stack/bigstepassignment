package com.venom.test.view;


import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.venom.test.R;
import com.venom.test.model.BaseActivity;
import com.venom.test.util.CircularNetworkImageView;
import com.venom.test.util.Constants;
import com.venom.test.model.Media;
import com.venom.test.util.MyVolleyRequest;

public class MusicInfoActivity extends BaseActivity {

    private Media media;
    private ImageButton playNpause;
    private TextView details;
    private CircularNetworkImageView circularNetworkImageView;
    private ImageLoader mImageLoader;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;
    private boolean playPause;
    private boolean initialStage = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        media = getIntent().getParcelableExtra(Constants.MUSIC);
        if (media == null) {
            onBackPressed();
            return;
        }
        mImageLoader = MyVolleyRequest.getInstance(this.getApplicationContext()).getImageLoader();
        circularNetworkImageView = findViewById(R.id.cImageView);
        playNpause = findViewById(R.id.playNpause);
        details = findViewById(R.id.details);
        mImageLoader.get(media.getArtworkUrl60().replace("60x60bb", "200x200bb"), ImageLoader.getImageListener(circularNetworkImageView,
                R.mipmap.ic_launcher, android.R.drawable
                        .ic_dialog_alert));
        circularNetworkImageView.setImageUrl(media.getArtworkUrl60().replace("60x60bb", "200x200bb"), mImageLoader);
        details.setText(Html.fromHtml("<big><b>" + media.getTrackName() + "</b></big><br><br>" +
                "Artist Name : " + media.getArtistName() + "<br><br>" +
                "Collection Name : " + media.getCollectionName() + "<br><br>" +
                "Release Date : " + media.getReleaseDate() + "<br><br>" +
                "Cost : " + media.getTrackPrice() + " USD<br>"
        ));
        if (media.isStreamable()) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            progressDialog = new ProgressDialog(this);
            playNpause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!playPause) {
                        playNpause.setImageResource(android.R.drawable.ic_media_pause);

                        if (initialStage) {
                            new Player().execute(media.getPreviewUrl());
                        } else {
                            if (!mediaPlayer.isPlaying())
                                mediaPlayer.start();
                        }

                        playPause = true;

                    } else {
                        playNpause.setImageResource(android.R.drawable.ic_media_play);


                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                        }

                        playPause = false;
                    }
                }
            });
        } else {
            playNpause.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_music_info;
    }

    class Player extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            Boolean prepared = false;

            try {
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        initialStage = true;
                        playPause = false;
                        playNpause.setImageResource(android.R.drawable.ic_media_play);

                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });

                mediaPlayer.prepare();
                prepared = true;

            } catch (Exception e) {
                Log.e("MyAudioStreamingApp", e.getMessage());
                prepared = false;
            }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            mediaPlayer.start();
            initialStage = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Buffering...");
            progressDialog.show();
        }
    }
}
