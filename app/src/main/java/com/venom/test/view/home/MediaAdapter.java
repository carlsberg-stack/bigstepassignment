package com.venom.test.view.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.venom.test.R;
import com.venom.test.model.Media;
import com.venom.test.util.OnItemClickListener;

import java.util.List;

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MyViewHolder> {

    private final ImageLoader imageLoader;
    private final OnItemClickListener onItemClickListener;
    private List<Media> mediaList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView songName, collectionName;
        public NetworkImageView networkImageView;

        public MyViewHolder(View view) {
            super(view);
            songName = view.findViewById(R.id.songName);
            collectionName = view.findViewById(R.id.collectionName);
            networkImageView = view.findViewById(R.id.networkImageView);
        }
    }


    public MediaAdapter(List<Media> mediaList, ImageLoader imageLoader, OnItemClickListener onItemClickListener) {
        this.mediaList = mediaList;
        this.imageLoader = imageLoader;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_media, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Media media = mediaList.get(position);
        holder.songName.setText(media.getTrackName());
        holder.collectionName.setText(media.getCollectionName());
        imageLoader.get(media.getArtworkUrl60(), ImageLoader.getImageListener(holder.networkImageView,
                R.mipmap.ic_launcher, android.R.drawable
                        .ic_dialog_alert));
        holder.networkImageView.setImageUrl(media.getArtworkUrl60(), imageLoader);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onClick(media);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }
}