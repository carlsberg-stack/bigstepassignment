package com.venom.test.view.home;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.venom.test.view.VideoInfoActivity;
import com.venom.test.model.BaseFragment;
import com.venom.test.util.Constants;
import com.venom.test.model.Media;
import com.venom.test.util.OnItemClickListener;

public class VideoFragment extends BaseFragment implements OnItemClickListener {

    private MediaAdapter mAdapter;
    private Bundle bundle;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (communicator.getVideo() == null || communicator.getVideo().isEmpty()) {
            dna.setVisibility(View.VISIBLE);
        } else {
            dna.setVisibility(View.GONE);
            mAdapter = new MediaAdapter(communicator.getVideo(), communicator.getImageLoader(), this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }

    }

    @Override
    public void onClick(Media media) {
        communicator.insertMedia(media);
        bundle = new Bundle();
        bundle.putParcelable(Constants.VIDEO, media);
        communicator.fragStartActivity(VideoInfoActivity.class, bundle);
    }
}
