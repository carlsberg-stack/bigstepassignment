package com.venom.test.view.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.venom.test.view.home.HistoryFragment;
import com.venom.test.view.home.MusicFragment;
import com.venom.test.view.home.VideoFragment;
import com.venom.test.util.Constants;

public class TabAdapterO extends FragmentPagerAdapter {
    private static final String mTITLES[] = {Constants.MUSIC, Constants.VIDEO, Constants.HISTORY};
    private static int NUM_ITEMS = 3;

    public TabAdapterO(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show MusicFragment
                return new MusicFragment();
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new VideoFragment();
            case 2: // Fragment # 1 - This will show SecondFragment
                return new HistoryFragment();
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return mTITLES[position];
    }

}
