package com.venom.test.view.home;

import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.android.volley.toolbox.ImageLoader;
import com.google.android.material.tabs.TabLayout;
import com.venom.test.R;
import com.venom.test.model.AppLogic;
import com.venom.test.model.BaseActivity;
import com.venom.test.model.Communicator;
import com.venom.test.util.Constants;
import com.venom.test.model.HomeFragPresenter;
import com.venom.test.model.MVPActivity;
import com.venom.test.model.Media;
import com.venom.test.util.MyVolleyRequest;

import java.util.List;

public class HomeActivity extends MVPActivity<AppLogic.HomeFragPresenter> implements AppLogic.HomeFragView, Communicator {


    private TabAdapterO adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Media> music;
    private List<Media> video;
    private ImageLoader mImageLoader;

    @Override
    protected AppLogic.HomeFragPresenter createPresenter() {
        return new HomeFragPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageLoader = MyVolleyRequest.getInstance(this.getApplicationContext()).getImageLoader();
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab_layout);
        if (isInternetAvailable()) {
            presenter.getMedia(this);
        } else {
            setAdapter();
            tabLayout.getTabAt(2).select();
        }
    }

    private void setAdapter() {
        adapter = new TabAdapterO(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }


    // start communicator implementation
    @Override
    public List<Media> getMusic() {
        return music;
    }

    @Override
    public List<Media> getVideo() {
        return video;
    }

    @Override
    public List<Media> getHistory() {
        return presenter.getHistory(getApplicationContext());
    }

    @Override
    public void insertMedia(Media media) {
        presenter.insertMedia(media, getApplicationContext());
    }

    @Override
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    @Override
    public void fragToast(String msg) {
        showToast(msg);
    }

    @Override
    public void fragStartActivity(Class<? extends BaseActivity> aClass, Bundle bundle) {
        startActivity(aClass, bundle);
    }
    // end communicator implementation


    // start applog4ic  implementation
    @Override
    public void showFailureError(int message) {

    }

    @Override
    public void showProgress(int message) {

    }

    @Override
    public void dismiss() {

    }

    @Override
    public void responseMedia(List<Media> media, int mediaType) {
        if (mediaType == Constants.RESULT_TYPE_MUSIC) {
            this.music = media;
        } else {
            this.video = media;
        }
        if (music != null && video != null)
            setAdapter();
    }
    // start applog4ic implementation

}
