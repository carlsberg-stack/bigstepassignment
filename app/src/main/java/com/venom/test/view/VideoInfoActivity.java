package com.venom.test.view;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.venom.test.R;
import com.venom.test.model.BaseActivity;
import com.venom.test.util.Constants;
import com.venom.test.model.Media;

public class VideoInfoActivity extends BaseActivity {


    private Media media;
    private TextView details;
    private VideoView vidView;
    private View progress_horizontal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        media = getIntent().getParcelableExtra(Constants.VIDEO);
        if (media == null) {
            onBackPressed();
            return;
        }
        details = findViewById(R.id.details);
        vidView = findViewById(R.id.videoView);
        progress_horizontal = findViewById(R.id.progress_horizontal);


        details.setText(Html.fromHtml("<big><b>" + media.getTrackName() + "</b></big><br><br>" +
                "Artist Name : " + media.getArtistName() + "<br><br>" +
                "Collection Name : " + media.getCollectionName() + "<br><br>" +
                "Release Date : " + media.getReleaseDate() + "<br><br>" +
                "Cost : " + media.getTrackPrice() + " USD<br>"
        ));
        Uri vidUri = Uri.parse(media.getPreviewUrl());
        vidView.setVideoURI(vidUri);
        final MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(vidView);
        vidView.setMediaController(vidControl);
        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
//                vidView.start();
                progress_horizontal.setVisibility(View.GONE);
                vidControl.show(0);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_video_info;
    }


}
