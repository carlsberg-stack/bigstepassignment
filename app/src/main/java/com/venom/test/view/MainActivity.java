package com.venom.test.view;

import android.os.Bundle;
import android.os.Handler;

import com.venom.test.R;
import com.venom.test.model.BaseActivity;
import com.venom.test.view.home.HomeActivity;

public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(HomeActivity.class);
                onBackPressed();
            }
        }, 2000);

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }
}

