package com.venom.test.util;

import com.venom.test.model.Media;

public interface OnItemClickListener {
    void onClick(Media media);
}