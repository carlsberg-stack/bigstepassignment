package com.venom.test.util;

import org.json.JSONObject;

public interface ApiClientHandler {

    void onSuccess(JSONObject result, int resultType);

    void onFaliure(String error);
}
