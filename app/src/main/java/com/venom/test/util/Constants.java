package com.venom.test.util;

public class Constants {
    public static final String MSG_CHECK_NETWORK = "Please check your internet connection and restart app again";
    public static final String URL_MUSIC = "https://itunes.apple.com/search?term=Michael+jackson&media=music";
    public static final String URL_MUSICVIDEO = "https://itunes.apple.com/search?term=Michael+jackson&media=musicVideo";
    public static final int RESULT_TYPE_MUSIC = 0;
    public static final int RESULT_TYPE_MUSICVIDEO = 1;
    public static final String VIDEO = "Video";
    public static final String MUSIC = "Music";
    public static final String HISTORY = "History";
    public static final String KIND_MUSIC = "song";
    public static final String KIND_MUSICVIDEO = "music-video";

    // jdon
    public static final String artistName = "artistName";
    public static final String kind = "kind";
    public static final String trackId = "trackId";
    public static final String trackName = "trackName";
    public static final String collectionName = "collectionName";
    public static final String previewUrl = "previewUrl";
    public static final String artworkUrl60 = "artworkUrl60";
    public static final String trackPrice = "trackPrice";
    public static final String releaseDate = "releaseDate";
    public static final String isStreamable = "isStreamable";

    //tables
    public static final String TABLE_NAME = "table_media";
    public static final String COLUMN_TIMESTAMP = "time_stamp";
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + trackId + " INTEGER PRIMARY KEY,"
                    + artistName + " TEXT,"
                    + kind + " TEXT,"
                    + trackName + " TEXT,"
                    + collectionName + " TEXT,"
                    + previewUrl + " TEXT,"
                    + artworkUrl60 + " TEXT,"
                    + trackPrice + " REAL,"
                    + releaseDate + " TEXT,"
                    + isStreamable + " INTEGER,"
                    + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";
}
